var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    sassGlob    = require('gulp-sass-glob'),
    sourcemaps  = require('gulp-sourcemaps'),
    bower       = require('gulp-bower'),
    imagemin    = require('gulp-imagemin'),
    changed     = require('gulp-changed'),
    pngquant    = require('imagemin-pngquant'),
    uglify      = require('gulp-uglify')
    do_concat   = require('gulp-concat'),
    gutil       = require('gulp-util'),
    browserSync = require('browser-sync').create();

var config = {
  bowerDir : './bower_components/',
  site_url : "http://gulp-sass.dev",
  styles    : [
    './bower_components/bootstrap-sass/assets/stylesheets', 
    './bower_components/font-awesome/scss/'
  ],
  scripts   : [
    'js/vendor/*.js', 
    'js/*.js'
  ],
  fonts     : [
      './bower_components/font-awesome/fonts/**.*'
  ]
}

/** Instala components com bower **/
gulp.task('bower', function() { 
    return bower()
        .pipe(gulp.dest(config.bowerDir)) 
});

/** Copia os arquivos de font do font-awesome **/
gulp.task('icons', function() { 
    return gulp.src(config.fonts) 
        .pipe(gulp.dest('assets/fonts')); 
});

/** Compila SASS -> CSS e minifica  **/
gulp.task('sass', function () {
  gulp.src(['sass/**/*.scss'])
    .pipe(sassGlob())
    .pipe(sourcemaps.init())
    .pipe(sass({
        outputStyle: 'compressed',
        includePaths: config.styles
    }).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('assets/css'));
});

/** Minifica imagens e joga dentro da pasta prod/imgages  **/
gulp.task('images', function() {
    gulp.src('images/**/*')
        .pipe(changed('assets/images'))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('assets/images'))
});

/** Compress JS **/
gulp.task('compress', function() {
  gulp.src( config.scripts )
      .pipe(do_concat('app.min.js'))
      .pipe(uglify().on('error', gutil.log))
      .pipe(gulp.dest('assets/js'));
});

/** Inicia BrowserSync no watch e escuta arquivos js e css  **/
gulp.task('browser-sync', function() {
    browserSync.init(["assets/css/*.css", "assets/js/*.js"], {
        proxy: config.site_url
    });
});

gulp.task('default', ['bower', 'icons', 'sass', 'images', 'compress', 'browser-sync'], function () {
    gulp.watch('sass/**/*.scss', ['sass']);
    gulp.watch('images/**/*', ['images']);
    gulp.watch('js/**/*.js', ['compress']);
});